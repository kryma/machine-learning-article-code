from collections import Counter, defaultdict
import matplotlib.pyplot
import random

import psycopg2

def get_examples():
	conn = psycopg2.connect(database="nldb_new", user="postgres")
	cur = conn.cursor()

	cur.execute("""
		SELECT videos.id, ARRAY_AGG(isaac_resources.name), videos.likes
		FROM videos
			JOIN played_characters ON played_characters.video=videos.id
			JOIN played_floors ON played_floors.played_character=played_characters.id
			JOIN gameplay_events ON gameplay_events.played_floor=played_floors.id
			JOIN isaac_resources ON gameplay_events.resource_one=isaac_resources.id
		WHERE gameplay_events.event_type = 2
		GROUP BY videos.id
	""")
	examples = []
	for row in cur:
		examples += [(row[0], Counter(row[1]), row[2])]
		examples[-1][1]["_INTERCEPT_"] = 1
	return examples

def get_variance(examples):
	mean_likes = 0
	for example in examples:
		mean_likes += example[2]
	mean_likes /= len(examples)
	variance = 0
	for example in examples:
		variance += (example[2] - mean_likes) ** 2
	variance /= len(examples)
	return variance

def get_average_error(examples):
	mean_likes = 0
	for example in examples:
		mean_likes += example[2]
	mean_likes /= len(examples)
	error = 0
	for example in examples:
		error += abs(example[2] - mean_likes)
	error /= len(examples)
	return error


class LinearRegression:
	def __init__(self):
		self.MAX_ITERATIONS = 5000
		self.STEP_SIZE = 0.00001
		self.REGULARIZATION_FACTOR = 0.999999
		self._weights = defaultdict(float)

	def predict(self, example):
		"""Returns the prediction for a given example."""
		prediction = 0
		for key, count in example[1].items():
			prediction += count * self._weights[key]
		return prediction

	def mass_predict(self, examples):
		"""Returns the average error for a given example array."""
		error_sum = 0
		for example in examples:
			error_sum += abs(example[2] - self.predict(example))
		return error_sum / len(examples)

	def train(self, train_set, dev_set):
		train_set_errors = []
		dev_set_errors = []
		for iter in range(self.MAX_ITERATIONS):
			for example in train_set:
				error = example[2] - self.predict(example)
				for feature, value in example[1].items():
					self._weights[feature] += error * value * self.STEP_SIZE
				for feature in self._weights:
					self._weights[feature] *= self.REGULARIZATION_FACTOR
			print(f"Iteration {iter}: ", end="")
			train_set_errors += [self.mass_predict(train_set)]
			dev_set_errors += [self.mass_predict(dev_set)]
			print(f"Error on train set: {train_set_errors[-1]}, ", end="")
			print(f"error on dev set: {dev_set_errors[-1]}")
			print()
		return (train_set_errors, dev_set_errors)

TRAIN_SET_PART = 0.8

examples = get_examples()

random.seed(50)
random.shuffle(examples)


train_set, dev_set = \
		examples[:int(len(examples) * TRAIN_SET_PART)],\
		examples[int(len(examples) * TRAIN_SET_PART):]

average_error = get_average_error(dev_set)

print()
print()
print(f"Dev set average error: {average_error}")
print()

reg = LinearRegression()
train_err, dev_err = reg.train(train_set, dev_set)


print(sorted([(i[1], i[0]) for i in reg._weights.items()]))

matplotlib.pyplot.plot(train_err)
matplotlib.pyplot.plot(dev_err)
matplotlib.pyplot.show()
